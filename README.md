# freebsd-desktop



## Getting started

This rocinante template is to set up a FreeBSD desktop with Nvidia driver, lightdm, and i3 wm.

Once freebsd is installed do the following steps to run this template
        # pkg install rocinante git
        
        # rocinante bootstrap freebsd-desktop

        # rociante template freebsd-desktop

## list of main packages included in this template

        # Xorg
        # mate window manager
        # Nvidia video drivers
        # lightdm display manager
        # vim
        # terminator



IF you do not want Nvidia drivers then comment out line 40 to not install the driver in the loader.conf.  Also remove/comment out line 111 to not install nvidia drivers.

If you want a different window manager other that i3, then change the packages listed
on line 115 to remove the i3 packages and add the packages  for your favorite window manager, xfce, mate, etc.

If you want to add more packages to be installed either add the packages to line 109, or add another PKG line to the end
of the list of PKG statements.
        
